const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'PRANESH'
});

connection.query("create database if not exists adminDB", function (err, results) {
    if (err) {
        console.log("err:" + err);
    } else {
        console.log('created adminDB');
        createTable();
    }
});

connection.query("use adminDB", function (err, results) {
    if (err) {
        console.log(err);
    } else {
        console.log("using adminDB");
    }
});

function createTable() {
    const registeradmin = `CREATE TABLE IF NOT EXISTS admintable(
                                        username VARCHAR(20)  NOT NULL UNIQUE,
                                        password varchar(20) NOT NULL)`;
    connection.query(registeradmin, (err, result) => {
        if (err) {
            console.log(err);
        } else {
            console.log('table created');
    }
    });
}

module.exports = connection;